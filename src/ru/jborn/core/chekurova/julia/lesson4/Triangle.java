package ru.jborn.core.chekurova.julia.lesson4;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Triangle implements Figure {
    protected double vertixA;
    protected double vertixB;
    protected double vertixC;

    protected double sideATringle;
    protected double sideBTringle;
    protected double sideCTringle;

    protected boolean isRightTriangle;
    protected double mediana;

    Triangle(double sideATringle, double sideBTringle) {
        this.sideATringle = sideATringle;
        this.sideBTringle = sideBTringle;
        this.sideCTringle = calculatSideC();

        this.vertixA = calculatCornersA();
        this.vertixB = calculatCornersB();
        this.vertixC = calculatCornersC();
        this.mediana = medianaCalculation();
    }

    Triangle(double sideATringle, double sideBTringle, double sideCTringle) {
        this.sideATringle = sideATringle;
        this.sideBTringle = sideBTringle;
        this.sideCTringle = sideCTringle;
    }

    @Override
    public double areaFigure(double sideA, double sideB, double vertixA) {
        return (0.5 * (sideA * sideB * Math.sin(vertixA)));
    }

    @Override
    public double perimeterFigure(double sideA, double sideB, double sideC) {
        return sideA + sideB + sideC;
    }

    @Override
    public void correctNumberOfAngles(Double[] angels) {
        if (angels.length > 3) {
            System.out.println("Это уже не треугольник, а нечто большее");
        } else if (angels.length == 3) {
            System.out.println("У тебя есть треугольник, ты посчитал углы");
        }
    }

    public double getSideATringle() {
        return sideATringle;
    }

    public double getSideBTringle() {
        return sideBTringle;
    }

    public double getSideCTringle() {
        return sideCTringle;
    }

    public double getVertixA() {
        return vertixA;
    }

    public double getVertixB() {
        return vertixB;
    }

    public double getVertixC() {
        return vertixC;
    }

    boolean checkTringleSimilar(Triangle triangle) {
        if (this.vertixA == triangle.vertixA && this.vertixB == triangle.vertixB) {
            return true;
        } else if (this.vertixB == triangle.vertixB && this.vertixC == triangle.vertixC) {
            return true;
        } else if (this.vertixC == triangle.vertixC && this.vertixA == triangle.vertixA) {
            return true;
        } else {
            return false;
        }
    }

    private double calculatCornersA() {
        return new BigDecimal(Math.toDegrees(Math.acos(((Math.pow(this.getSideBTringle(), 2))
                + (Math.pow(this.getSideCTringle(), 2)) - (Math.pow(this.getSideATringle(), 2)))
                / (2 * this.getSideBTringle() * this.getSideCTringle())))).setScale(2, RoundingMode.UP).doubleValue();
    }

    private double calculatCornersB() {
        return new BigDecimal(Math.toDegrees(Math.acos(((Math.pow(this.getSideATringle(), 2))
                + (Math.pow(this.getSideCTringle(), 2)) - (Math.pow(this.getSideBTringle(), 2)))
                / (2 * this.sideBTringle * this.sideCTringle)))).setScale(2, RoundingMode.UP).doubleValue();
    }

    private double calculatCornersC() {
        return 180.0 - (this.getVertixA() + this.getVertixB());
    }


    private double calculatSideC() {
        return this.sideCTringle = Math.sqrt(Math.pow(this.getSideATringle(), 2)
                + Math.pow(this.getSideBTringle(), 2));
    }

    private double medianaCalculation() {
        return Math.sqrt(2 * Math.pow(this.getSideATringle(), 2) + 2 * Math.pow(this.getSideBTringle(), 2)
                - Math.pow(this.getSideCTringle(), 2)) / 2;
    }
}