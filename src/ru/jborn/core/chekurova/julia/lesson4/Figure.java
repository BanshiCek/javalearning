package ru.jborn.core.chekurova.julia.lesson4;

public interface Figure {
    double areaFigure(double sideA, double sideB, double vertixAB);

    double perimeterFigure(double sideA, double sideB, double sideC);

    void correctNumberOfAngles(Double[] arg);
}

