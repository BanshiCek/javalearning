package ru.jborn.core.chekurova.julia.lesson4;

public class RightTriangle extends Triangle {
    boolean checkTringleSimilar;

    RightTriangle(double sideA, double sideB) {
        super(sideA, sideB);
        this.vertixA = this.getVertixA();
        this.vertixB = calculatCornersB();
        this.vertixC = calculatCornersC();
        this.isRightTriangle = chekRightTringal();
    }

    @Override
    public double getVertixC() {
        return vertixC;
    }

    @Override
    public String toString() {
        return "RightTriangle " + " sideA = " + getSideATringle()
                + " sideB = " + getSideBTringle()
                + " sideC = " + getSideCTringle()
                + "\n"
                + " vertixA = " + getVertixA()
                + " vertixB = " + getVertixB()
                + " vertixC = " + getVertixC()
                + "\n"
                + " isRightTringle = " + this.isRightTriangle
                + "\n"
                ;
    }

    private boolean chekRightTringal() {
        if (this.vertixC == 90.0 || this.vertixB == 90.0 || this.vertixA == 90.0) {
            System.out.println("Треугольник прямоугольный");
            return true;
        } else {
            System.out.println("Треугольник не прямоугольный");
            return false;
        }
    }

    private double calculatCornersB() {
        return 90.0 - this.getVertixA();
    }

    private double calculatCornersC() {
        return 90.0;
    }
}