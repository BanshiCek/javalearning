package ru.jborn.core.chekurova.julia.lesson3;

import java.util.Scanner;

/**
 * Дано слово. Проверить, является ли оно "перевертышем" (перевертышем называется слово,
 * читаемое одинаково как с начала, так и с конца).
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word :");
        String word = scanner.nextLine();
        System.out.println(flipString(word));
    }

    private static Boolean flipString(String text) {
        text = text.toLowerCase();
        String stringInvert = "";

        for (int i = text.length() - 1; i >= 0; i--) {
            stringInvert = stringInvert.concat(String.valueOf(text.charAt(i)));
        }
        return text.equals(stringInvert);
    }
}
