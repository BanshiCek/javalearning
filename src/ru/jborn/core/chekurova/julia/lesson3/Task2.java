package ru.jborn.core.chekurova.julia.lesson3;

/**
 * Дано слово. Добавить к нему в начале и конце столько звездочек, сколько букв в этом слове.
 */
public class Task2 {
    public static void main(String[] args) {
        System.out.println(addCorrectStar("cat ca"));
        System.out.println(addCorrectStar("cat cats"));
        System.out.println(addCorrectStar("a c"));
    }

    private static String addCorrectStar(String text) {
        StringBuilder stringBuild = new StringBuilder("");

        for (char chars : text.toCharArray()) {

            if (chars != ' ') {
                stringBuild.append("*");
            }
        }
        return stringBuild + text + stringBuild;
    }
}
