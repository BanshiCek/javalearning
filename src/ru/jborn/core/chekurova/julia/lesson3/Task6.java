package ru.jborn.core.chekurova.julia.lesson3;

import java.util.Scanner;

/**
 * Дан текст, представляющий собой десятичную запись целого числа. Вычислить сумму цифр этого числа
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите целое число : ");
        String text = scanner.nextLine();
        System.out.println(countSumm(text));
    }

    private static int countSumm(String text) {
        int sum = 0;
        for (int i = 0; i <= text.length() - 1; i++) {
            sum += Character.getNumericValue(text.charAt(i));
        }
        return sum;
    }
}
