package ru.jborn.core.chekurova.julia.lesson3;

/**
 * Дан текст. В нем слова разделены одним пробелом
 * (начальные и конечные пробелы и символ "-" в тексте отсутствуют). Определить количество слов в тексте.
 */

public class Task4 {
    public static void main(String[] args) {
        System.out.println(countingWords("Я хочу очень сильно домой"));
        System.out.println(countWords("Я хочу очень сильно домой"));
        System.out.println(countingWords("Бесплатный сервис Google позволяет мгновенно переводить слова, фразы и веб-страницы с английского на более чем 100 языков и обратно"));
        System.out.println(countWords("Бесплатный сервис Google позволяет мгновенно переводить слова, фразы и веб-страницы с английского на более чем 100 языков и обратно"));
    }

    private static int countingWords(String text) {
        char ch = ' ';
        int numberWords = 0;

        for (int count = 0; count < text.length() - 1; count++) {

            if (ch == text.charAt(count)) {
                numberWords++;
            }
        }
        return numberWords + 1;
    }

    private static int countWords(String text) {
        int resultSumWord = 0;

        for (char chars : text.toCharArray()) {

            if (chars == ' ') {
                resultSumWord++;
            }
        }
        return resultSumWord + 1;
    }
}
