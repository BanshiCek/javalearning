package ru.jborn.core.chekurova.julia.lesson3;

/**
 * Дан текст и символ. Определить количество данного символа в тексте.
 */
public class Task3 {
    public static void main(String[] args) {
        System.out.println(gettingNumberCharacters("cat love cat123", "a"));
        System.out.println(gettingNumberCharacters("Я хочу очень сильно домой", "о"));
        System.out.println(getNumberChar("Я хочу очень сильно домой", "о"));
        System.out.println(getNumberChar("cat 123 love cat123", "a"));
    }

    private static int gettingNumberCharacters(String text, String sign) {
        char ch = sign.charAt(0);
        int count = 0;

        for (int i = 0; i < text.length() - 1; i++) {
            if (ch == text.charAt(i)) {
                count++;
            }
        }
        return count;
    }

    private static int getNumberChar(String text, String chars) {
        int countChar = 0;

        for (char charString : text.toCharArray()) {
            if (charString == chars.charAt(0)) {
                countChar++;
            }
        }
        return countChar;
    }
}