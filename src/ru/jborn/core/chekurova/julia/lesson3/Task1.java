package ru.jborn.core.chekurova.julia.lesson3;

import java.util.Scanner;

/**
 * Составить программу, которая печатает заданное слово, начиная с последней буквы
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the string :");
        String text1 = scanner.nextLine();
        printString(text1);
        printStringBuffer(text1);
    }

    private static void printString(String text) {
        text = text.toLowerCase();
        String emptyLine = "";

        for (int coun = text.length() - 1; coun >= 0; coun--) {
            emptyLine = emptyLine.concat(String.valueOf(text.charAt(coun)));
        }
        System.out.println(emptyLine);
    }

    private static void printStringBuffer(String textBuffer) {
        StringBuilder text = new StringBuilder("");

        for (int count = textBuffer.length() - 1; count >= 0; count--) {
            text.append(textBuffer.charAt(count));
        }
    }
}
