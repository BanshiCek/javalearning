package ru.jborn.core.chekurova.julia.lesson2;

import java.util.Scanner;

/**
 * Дано двузначное число. Найти сумму его цифр;
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = in.nextInt();

        if (String.valueOf(number).length() != 2) {
            System.out.println("Число не двухзначное");
        } else {
            System.out.println(getSum(number));
        }
    }

    private static int getSum(int i) {
        return (i / 10) + (i % 10);
    }
}
