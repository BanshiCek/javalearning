package ru.jborn.core.chekurova.julia.lesson2;

import java.util.Scanner;

/**
 * Напечатать таблицу умножения на число n (значение n вводится с клавиатуры; n от 1 до 9).
 */
public class Task11 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println(" Enter some number :");
        int number = in.nextInt();

        if (number < 0) {
            System.out.println("Число отрицательное так не должно быть!");
        } else {
            multiplicationTable(number);
        }
    }

    private static void multiplicationTable(int numberMultiplication) {
        for (int b = 1; b < 10; b++) {
            int res = numberMultiplication * b;
            System.out.println(numberMultiplication + " * " + b + " = " + res);
        }
    }
}
