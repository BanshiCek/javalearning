package ru.jborn.core.chekurova.julia.lesson2;

/**
 * Дано вещественное число x. Вычислить f (x)
 */
public class Task9 {
    public static void main(String[] args) {
        System.out.println(computation(1.1));
        System.out.println(computation(-1.1));
        System.out.println(computation(0.3));
    }

    private static double computation(double numberComputtion) {
        if (numberComputtion <= 0) {
            return 0;
        } else if (numberComputtion > 0 && numberComputtion <= 1) {
            return numberComputtion;
        } else {
            return Math.pow(numberComputtion, 2);
        }
    }
}
