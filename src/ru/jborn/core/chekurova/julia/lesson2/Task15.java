package ru.jborn.core.chekurova.julia.lesson2;

import java.util.Scanner;

/**
 * Вывести пирамиду
 *   *
 *  ***
 * *****
 * Где также кол-во звезд на нижнем уровне, передается параметром
 */
public class Task15 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println(" Enter some number :");
        int number = in.nextInt();

        if (number % 2 == 0) {
            number++;
        }
        drawPyramid(number);
    }

    private static void drawPyramid(int lines) {
        int spaceCounter;
        int starCounter;
        starCounter = 1;
        spaceCounter = lines / 2;

        for (int i = 1; i <= (lines / 2 + 1); i++) {

            for (int counterSpace = 1; counterSpace <= spaceCounter; counterSpace++) {
                System.out.print(" ");
            }

            for (int counterStars = 1; counterStars <= starCounter; counterStars++) {
                System.out.print("*");
            }

            System.out.print("\n");
            spaceCounter--;
            starCounter += 2;
        }
    }
}