package ru.jborn.core.chekurova.julia.lesson2;

/**
 * Рассчитать значение у при заданном значении х:
 */
public class Task2 {
    public static void main(String[] args) {
        System.out.println(getY(0.5));
        System.out.println(getY(1.5));
        System.out.println(getY(0));
    }

    private static double getY(double parameter) {
        if (parameter > 0) {
            return Math.pow(Math.sin(parameter), 2);
        } else {
            return (1 - 2 * Math.sin(Math.pow(parameter, 2)));
        }
    }
}
