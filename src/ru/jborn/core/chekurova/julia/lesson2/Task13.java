package ru.jborn.core.chekurova.julia.lesson2;

import java.util.Scanner;

/**
 * Вывести треугольник в консоль. Где кол-во звездочек на последнем уровне передается параметром
 * *
 * **
 * ***
 * .....
 * ***************
 */
public class Task13 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter some number :");
        int number = in.nextInt();

        if (number < 0) {
            drawTriangle(Math.abs(number));
        } else if (number == 0) {
            number += 1;
            drawTriangle(number);
        } else {
            drawTriangle(number);
        }
    }

    private static void drawTriangle(int counts) {
        for (int num1 = 1; num1 <= counts; num1++) {

            for (int num2 = 1; num2 <= num1; num2++) {

                if (num2 == num1) {
                    System.out.print("*" + "\n");
                } else {
                    System.out.print("*");
                }
            }
        }
    }
}
