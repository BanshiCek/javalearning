package ru.jborn.core.chekurova.julia.lesson2;

import java.util.Scanner;

/**
 * Определить максимальное и минимальное значения из двух различных чисел.
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the first number:");
        int numberFirst = in.nextInt();
        System.out.println("Enter the second number:");
        int numberSecond = in.nextInt();
        getMax(numberFirst, numberSecond);
    }

    private static void getMax(int i, int k) {
        if (i > k) {
            System.out.println("Max = " + i + " Min = " + k);
        } else {
            System.out.println("Max = " + k + " Min = " + i);
        }
    }
}
