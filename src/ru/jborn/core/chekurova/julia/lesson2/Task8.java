package ru.jborn.core.chekurova.julia.lesson2;

import java.util.Scanner;

/**
 * Дано трехзначное число. Определить, есть ли среди его цифр одинаковые.
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = in.nextInt();

        if (String.valueOf(number).length() != 3) {
            System.out.println("Число не трехзначное");
        } else {
            equalityTest(number);
        }
    }

    private static void equalityTest(int numberEquality) {
        int hundred = numberEquality / 100;
        int tenths = numberEquality / 10 % 10;
        int lastNumber = (numberEquality % 100) % 10;

        if ((hundred == lastNumber) || (hundred == tenths) || (tenths == lastNumber)) {
            System.out.println("Есть совпадения в цифрах");
        } else {
            System.out.println("Числа разные");
        }
    }
}
