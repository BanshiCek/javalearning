package ru.jborn.core.chekurova.julia.lesson2;

/**
 * Если целое число m делится нацело на целое число n, то вывести на экран частное от деления,
 * в противном случае вывести сообщение "m на n нацело не
 * делится".
 */
public class Task6 {
    public static void main(String[] args) {
        float res;

        res = fissionResidue(12, 5);
        divisionWhole(res);
        res = fissionResidue(16, 8);
        divisionWhole(res);
    }

    private static float fissionResidue(float dividend, float divider) {
        float result = dividend % divider;
        if (result == 0) {
            return result;
        } else {
            return 0;
        }
    }

    private static void divisionWhole(float valueDivision) {
        if (valueDivision != 0) {
            System.out.println("m на n нацело не делится");
        } else {
            System.out.println("Частное от деления = " + valueDivision);
        }
    }
}
