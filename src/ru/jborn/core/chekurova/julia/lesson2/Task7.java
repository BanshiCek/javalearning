package ru.jborn.core.chekurova.julia.lesson2;

import java.util.Scanner;

/**
 * Дано трехзначное число. Выяснить, является ли оно палиндромом ("перевертышем"),
 * т. е. таким числом, десятичная запись которого читается одинаково слева направо и справа налево.
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = in.nextInt();

        if (String.valueOf(number).length() != 3) {
            System.out.println("Число не трехзначное");
        } else {
            checkPalindrome(number);
        }
    }

    private static void checkPalindrome(int number) {
        int hundred = number / 100;
        int unitPart = (number % 100) % 10;

        if (hundred == unitPart) {
            System.out.println("Число палиндромом");
        } else {
            System.out.println("Число не палиндромом");
        }
    }
}
