package ru.jborn.core.chekurova.julia.lesson2;

import java.util.Scanner;

/**
 * Известны два расстояния: одно в километрах, другое — в футах
 * (1 фут 0,305 м). Какое из расстояний меньше?
 */
public class Task4 {
    private final static double MILI = 0.305;

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter the kilometers: ");
        int numberKilometers = in.nextInt();
        System.out.println("Enter the feet: ");
        int numberFeet = in.nextInt();
        System.out.println(getMin(numberKilometers, numberFeet));
    }

    private static double getMin(double km, double feet) {
        feet = feet * MILI;

        if (km < feet) {
            return km;
        } else {
            return feet;
        }
    }
}
