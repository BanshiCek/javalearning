package ru.jborn.core.chekurova.julia.lesson5.clone;

import java.time.LocalDate;
import java.time.Month;

/**
 * Есть 2 класса Car, Driver. Car имеет свойства: модель, марка, дата выпуска (год, месяц, день)
 * , гос номер, пробег в км. Driver имеет свойства: имя, фамилия, отчество, дата рождения (год, месяц, день),
 * стаж вождения (полных лет). У Driver есть поле Car car. Реализовать swallow copy и deep copy в обоих классах.
 * Deep copy в новом методе класса, deepClone()
 */
public class Task1 {
    public static void main(String[] args) throws CloneNotSupportedException {
        LocalDate dateOfRelease1 = LocalDate.of(2017, Month.JANUARY, 30);
        LocalDate dateOfRelease2 = LocalDate.of(2007, Month.JUNE, 15);
        LocalDate dateOfBirth1 = LocalDate.of(1999, Month.APRIL, 1);
        LocalDate dateOfBirth2 = LocalDate.of(1987, Month.JUNE, 23);

        Car car1 = new Car("Пукалка", "BMW", dateOfRelease1, "E123TR", 15);
        Car car2 = new Car("Звезда", "Audi", dateOfRelease2, "X123TR", 0);

        Driver driver1 = new Driver("Вася", "Пупкин", "Васильевич", dateOfBirth1, 3, car1);

        Car carBMW = car1.deepClone();
        Driver driver2 = driver1.deepClone();
        System.out.println(driver2.toString());

        driver1.setName("B");
        driver1.setSurname("L");
        driver1.setPatronymic("AAAA");
        driver1.setDateBirth(dateOfBirth2);
        driver1.setCar(car2);

        System.out.println(driver1.toString());

        car1.setModel("Трактор");
        car1.setMark("Драндулет");

        System.out.println(driver2.toString());
        System.out.println(driver1.toString());
        System.out.println(car1.toString());
        System.out.println(carBMW.toString());
    }
}
