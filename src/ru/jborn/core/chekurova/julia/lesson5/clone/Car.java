package ru.jborn.core.chekurova.julia.lesson5.clone;

import java.time.LocalDate;
import java.time.Month;

public class Car implements Cloneable {
    private String model;
    private String mark;
    private LocalDate releaseDate;
    private String stateNumber;
    private int mileage;

    Car(String model, String mark, LocalDate releaseDate, String stateNumber, int mileage) {
        this.model = model;
        this.mark = mark;
        this.releaseDate = releaseDate;
        this.stateNumber = stateNumber;
        this.mileage = mileage;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(int ear, Month month, int day) {
        this.releaseDate = LocalDate.of(ear, month, day);
    }

    public String getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(String stateNumber) {
        this.stateNumber = stateNumber;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String toString() {
        return "Car {" + " model = " + model + " mark = " + mark + "\n" +
                " releaseDate = " + releaseDate +
                " stateNumber = " + stateNumber + "\n" +
                " mileage = " + mileage + " }";
    }

    @Override
    public Car clone() throws CloneNotSupportedException {
        return (Car) super.clone();
    }

    public Car deepClone() throws CloneNotSupportedException {
        Car clone = (Car) super.clone();
        clone.model = model;
        clone.mark = mark;
        clone.releaseDate = releaseDate;
        return clone;
    }
}
