package ru.jborn.core.chekurova.julia.lesson5.clone;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

public class Driver implements Cloneable {
    private String name;
    private String surname;
    private String patronymic;
    private LocalDate dateBirth;
    private int drivingExperience;
    private Car car;

    Driver(String name, String surname, String patronymic, LocalDate dateBirth, int drivingExperience, Car car) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.dateBirth = dateBirth;
        this.drivingExperience = drivingExperience;
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public LocalDate getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(LocalDate dateBirth) {
        this.dateBirth = dateBirth;
    }

    public int getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(int drivingExperience) {
        this.drivingExperience = drivingExperience;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setDateBirth(int ear, Month month, int day) {
        this.dateBirth = LocalDate.of(ear, month, day);
    }

    @Override
    public String toString() {
        return "Driver{" + " name = " + name + " surname = " + surname +
                " patronymic = " + patronymic + "\n" +
                " dateBirth = " + dateBirth +
                " drivingExperience = " + drivingExperience +
                " car = " + car + " }" + "\n";
    }

    @Override
    public Driver clone() throws CloneNotSupportedException {
        return (Driver) super.clone();
    }

    public Driver deepClone() throws CloneNotSupportedException {
        Driver clone = (Driver) super.clone();
        clone.name = name;
        clone.surname = surname;
        clone.patronymic = patronymic;
        clone.dateBirth = dateBirth;
        clone.car = car;
        return clone;
    }
}
